﻿using System;

namespace DBCommandsUpdater
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Updating the script files has started...\n");

            ScriptCommentsAdder.AddComments();

            Console.WriteLine("\nAdding comments has finished...");
        }
    }
}
