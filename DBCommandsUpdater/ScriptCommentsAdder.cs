﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Configuration;

namespace DBCommandsUpdater
{
    public static class ScriptCommentsAdder
    {
        public static void AddComments()
        {
            var pathToRead = ConfigurationManager.AppSettings.Get("pathToRead");
            string delimeter = ",|,";
            var endsWithDelimiter = "]]";

            try
            {
                var descriptions = File.ReadAllLines(pathToRead);

                for (int i = 0; i < descriptions.Length; i++)
                {
                    var descriptionsForTable = new Dictionary<(string, string), List<string>>();

                    var line = descriptions[i];

                    while (true)
                    {
                        var descriptionParts = line.Split(delimeter);
                        var tableName = descriptionParts[0];
                        var columnName = descriptionParts[1];

                        var comments = new List<string> { descriptionParts[2] };

                        if (!(line.EndsWith(endsWithDelimiter)))
                        {
                            while (true)
                            {
                                i++;
                                line = descriptions[i];
                                comments.Add(line);

                                if (line.EndsWith(endsWithDelimiter))
                                {
                                    break;
                                }
                            }
                        }

                        descriptionsForTable.Add((tableName, columnName), comments);

                        if (i == descriptions.Length - 1 || !((descriptions[i + 1]).StartsWith($"{tableName}{delimeter}")))
                        {
                            break;
                        }

                        i++;
                        line = descriptions[i];
                    }

                    UpdateScript(descriptionsForTable);
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine($"{pathToRead} file cannot be found.\n");
            }
            catch (IOException e)
            {
                Console.WriteLine($"An exception occurred:\nError code: {e.HResult & 0x0000FFFF}\nMessage: {e.Message}\n");
            }
        }

        public static void UpdateScript(Dictionary<(string, string), List<string>> descriptionsForTable)
        {
            string rootPathToWrite = ConfigurationManager.AppSettings.Get("rootPathToWrite");
            string fileExtension = ".sql";
            string tableName = descriptionsForTable.Keys.First().Item1;

            string pathToWrite = String.Concat(rootPathToWrite, tableName, fileExtension);

            try
            {
                var scriptLines = new List<string>(File.ReadAllLines(pathToWrite));

                Console.WriteLine($"Updating {tableName}{fileExtension} file\n");

                foreach (var description in descriptionsForTable)
                {
                    var columnName = description.Key.Item2;
                    string subToSearch = (columnName == "") ? "create table" : $"[{columnName}]";

                    int index = scriptLines.FindIndex(s => s.Contains(subToSearch, StringComparison.InvariantCultureIgnoreCase));

                    if (index > -1)
                    {
                        int currentIndex = index - 1;

                        while (currentIndex >= 0 && (scriptLines[currentIndex]).StartsWith("--"))
                        {
                            scriptLines.RemoveAt(currentIndex);
                            currentIndex--;
                        }

                        index = currentIndex + 1;
                        var commentLines = MakeCommentLines(description.Value);

                        scriptLines.InsertRange(index, commentLines);
                    }
                }

                File.WriteAllText(pathToWrite, "");
                File.WriteAllLines(pathToWrite, scriptLines);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine($"{tableName}{fileExtension} file cannot be found.\n");
            }
            catch (IOException e)
            {
                Console.WriteLine($"An exception occurred:\nError code: {e.HResult & 0x0000FFFF}\nMessage: {e.Message}\n");
            }
        }

        public static List<string> MakeCommentLines(List<string> comments)
        {
            var commentLines = comments.Select<string, string>(commentLine =>
            {
                var comment = commentLine.TrimStart('[').TrimEnd(']');
                return $"-- {comment}";
            }).ToList();

            return commentLines;
        }
    }
}
